#Data Analytics Group Module
#Started 20/10/2020
#This module will contain our implementations of various algorithms and
#formula in order to help with the Data Analytics requirements.

#TODO: Write unit tests for these methods, D Harding

import math


#Calculate a min/max normalised value
def Normalise(value, minValue, maxValue):
    return(value - minValue) / (maxValue - minValue)

#Calculates the ZScore normalised value, useful for when dealing with outliers
def ZScoreNormalise(value, mean, standardDeviation):
    return((value - mean) / standardDeviation)

#Calculates the standard deviation for a given set of values
def CalcStandardDeviation(listOfValues):
    mean = CalcMean(listOfValues)

    listOfResults = []

    for var in listOfValues:
        listOfResults.append((var - mean) ** 2)

    return math.sqrt(sum(listOfResults) / len(listOfResults))

#Calculates the mean for a given set of values
def CalcMean (listOfValues):
    return sum(listOfValues)/len(listOfValues)





##Validation helpers
#While not strickly data analytics these methods will help
#with identifying data types

def IsInt(value):
    try: 
        int(value)
        return True
    except ValueError:
        return False

def IsFloat(value):
    try:
        float(value)
        return True
    except ValueError:
        return False
